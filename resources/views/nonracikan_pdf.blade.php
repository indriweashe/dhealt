<!DOCTYPE html>
<html>
<head>
	<title>Resep</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Resep Obat</h4>
	</center>

	<table class='table table-bordered'>
		<thead>
			<tr>
            <th>Nama Obat</th>
            <th>Jumlah Obat</th>
            <th>Keterangan</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{{$obatalkes}}</td>
            <td>{{$qty}}</td>
            <td>{{$signa}}</td>
          </tr>
		</tbody>
	</table>

</body>
</html>