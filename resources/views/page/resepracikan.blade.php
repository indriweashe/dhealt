@extends('layout/layout')
@section('konten')
 <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Input Resep Racikan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Resep</a></li>
              <li class="breadcrumb-item active">Racikan</li>
            </ol>
          </div>
          <br>
          <div class="col-sm-12">
            @if(Session::has('error'))
              <div class="alert alert-danger">
                {{ Session::get('error') }}
                  @php
                      Session::forget('error');
                  @endphp
              </div>
              @endif
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Form Input Resep</h3>
          </div>
          <div class="card-body">
            <div class="row">
              <form role="form" action="{{ url('resep/racikan/post')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                  <div class="form-group">
                    <label>Nama Resep</label>
                    <div class="input-group">
                      <input type="text" class="form-control" name="nama" id="nama" required="required">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-5">
                      <label>Pilih Obat </label>
                      <select class="form-control select2bs4" name="obat[]"  required="required"  id="obat">
                        <option value="">Pilih Obat</option>
                        @foreach($obatalkes as $p)
                        <option value="{{$p->obatalkes_kode}}">{{$p->obatalkes_nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-5">
                      <label>Ketentuan Pemberian Obat</label>
                      <select class="form-control select2bs4" name="signa[]"  required="required"  id="signa">
                        <option value="">Pilih Signa</option>
                        @foreach($signa as $s)
                        <option value="{{$s->signa_kode}}">{{$s->signa_nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label>Qty</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Qty</span>
                        </div>
                        <input type="number" class="form-control" name="qty[]" id="qty" required="required">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <p style="font-size: 12px; color: green" class="stok"></p>
                    <a href="#" onclick="addOtherObat()"><i class="fas fa-plus fa-sm"></i> Tambah Obat</a>
                  </div>
                  
                  <od></od>
                  <br>
                  <div class="input-group">
                    <button class="btn btn-sm btn-info" id="btn_submit" type="submit"><i class="fa fa-check"></i> | Proses</button>
                  </div>

                  <p style="font-size: 12px; color: red" class="alert_stock"></p>
              </form>
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="card card-default">
            <div class="card-header">
              <h3 class="card-title">Tabel Obat</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <table class="display table table-striped table-hover yajra-datatable" cellspacing="0" width="100%" id="yajra-datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Stok</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
  </div>
@endsection

@section('footer')

<script type="text/javascript">
  $(function () {
    
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('obatalkes/list') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'obatalkes_kode', name: 'obatalkes_kode'},
            {data: 'obatalkes_nama', name: 'obatalkes_nama'},
            {data: 'stok', name: 'stok'},
        ]
    });
    
  });

  function addOtherObat() {
    $("od").append("<div class='row'><div class='col-md-5'><select class='form-control select2bs4' name='obat[]'  required='required'  id='obat'><option value=''>Pilih Obat</option>@foreach($obatalkes as $p)<option value='{{$p->obatalkes_kode}}'>{{$p->obatalkes_nama}}</option>@endforeach</select></div><div class='col-md-5'><select class='form-control select2bs4' name='signa[]'  required='required'  id='signa'><option value=''>Pilih Signa</option>@foreach($signa as $s)<option value='{{$s->signa_kode}}'>{{$s->signa_nama}}</option>@endforeach</select></div><div class='col-md-2'><div class='input-group'><div class='input-group-prepend'><span class='input-group-text'>Qty</span></div><input type='number' class='form-control' name='qty[]' id='qty' required='required'></div></div></div>");
  }
</script>
@endsection



 
