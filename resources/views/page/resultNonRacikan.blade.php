@extends('layout/layout')
@section('konten')
 <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Resep</a></li>
              <li class="breadcrumb-item active">Result</li>
            </ol>
          </div>
          <br>
          <div class="col-sm-12">              
            @if(Session::has('error'))
              <div class="alert alert-danger">
                  {{ Session::get('error') }}
                  @php
                      Session::forget('error');
                  @endphp
              </div>
            @endif
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Resep</h3>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <table class="display table table-striped table-hover" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>Nama Obat</th>
                        <th>Jumlah Obat</th>
                        <th>Keterangan</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{$obatalkes}}</td>
                        <td>{{$qty}}</td>
                        <td>{{$signa}}</td>
                      </tr>
                    </tbody>
                    <thead>
                      <tr>
                        <td colspan="3">
                          <form role="form" action="{{ url('resep/nonracikan/print')}}" target="_blank" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="name" value="{{$obatalkes}}">
                            <input type="hidden" name="qty" value="{{$qty}}">
                            <input type="hidden" name="signa" value="{{$signa}}">
                            <button type="submit" class="btn btn-sm btn-primary float-sm-right"><i class="fas fa-print"></i> Print</button>
                          </form>
                        </td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
  </div>
@endsection