<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ObatalkesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ObatalkesController@index');
Route::get('obatalkes/list', 'ObatalkesController@getObatalkes');
Route::get('resep/racikan', 'ObatalkesController@resepRacikan');
Route::get('resep/nonracikan', 'ObatalkesController@resepNonRacikan');
Route::get('obatalkes/stock/{kode}', 'ObatalkesController@getStockObat');

Route::post('resep/nonracikan/post', 'ResepController@postNonRacikan');
Route::post('resep/racikan/post', 'ResepController@postRacikan');
Route::post('resep/nonracikan/print', 'ResepController@NonRacikan_pdf');
Route::post('resep/racikan/print', 'ResepController@Racikan_pdf');