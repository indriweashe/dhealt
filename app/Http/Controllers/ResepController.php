<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Obatalkes;
use App\Models\Signa;

use PDF;
class ResepController extends Controller
{

    public function postNonRacikan(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'obat'     => 'required',
                'signa'    => 'required',
                'qty'      => 'required|numeric'
            ]);

        if ($validator->fails()) {
            return back()->with('error', 'Pastikan Semua Field Terisi Dengan Benar.');
        }

        $dt_obat    = Obatalkes::select('obatalkes_nama','stok')
                           ->where('obatalkes_kode', '=', $request->obat)
                           ->first();

        $dt_signa   = Signa::select('signa_nama')
                           ->where('signa_kode', '=', $request->signa)
                           ->first();

        if ($dt_obat->stok < $request->qty) {
            return back()->with('error', 'Stok obat tidak cukup.');
        }
        $stokNew    = $dt_obat->stok-$request->qty; 
        $update     = Obatalkes::where('obatalkes_kode', '=', $request->obat)
                        ->update(['stok' => $stokNew]);

        return view('page/resultNonRacikan',   [
                                        'obatalkes' => $dt_obat->obatalkes_nama, 
                                        'signa'     => $dt_signa->signa_nama,
                                        'qty'       => $request->qty
                                    ]);

    }

    public function postRacikan(Request $request)
    {
        $arrayObat  = $request->get('obat');
        $arraySigna = $request->get('signa');
        $arrayQty   = $request->get('qty');
        $data = array();
        for ($i=0; $i < count($arrayObat); $i++) { 
            $dt_obat    = Obatalkes::select('obatalkes_nama','stok')
                           ->where('obatalkes_kode', '=', $arrayObat[$i])
                           ->first();

            $dt_signa   = Signa::select('signa_nama')
                               ->where('signa_kode', '=', $request->signa)
                               ->first();

            if ($dt_obat->stok < $arrayQty[$i]) {
                return back()->with('error', 'Stok obat tidak cukup.');
            }

            $detail = array(
                                'kode_obat' => $arrayObat[$i],
                                'obat'      => $dt_obat->obatalkes_nama,
                                'kode_signa'=> $arraySigna[$i],
                                'signa'     => $dt_signa->signa_nama,
                                'qty'       => $arrayQty[$i]
                            );
            array_push($data, $detail);
        }

        for ($i=0; $i < count($arrayObat); $i++) { 
            $dt_obat    = Obatalkes::select('obatalkes_nama','stok')
                           ->where('obatalkes_kode', '=', $arrayObat[$i])
                           ->first();

            $dt_signa   = Signa::select('signa_nama')
                               ->where('signa_kode', '=', $request->signa)
                               ->first();

            if ($dt_obat->stok < $arrayQty[$i]) {
                return back()->with('error', 'Stok obat tidak cukup.');
            }

            $stokNew    = $dt_obat->stok-$arrayQty[$i]; 
            $update     = Obatalkes::where('obatalkes_kode', '=', $arrayObat[$i])
                            ->update(['stok' => $stokNew]);
        }

        
        return view('page/resultRacikan',   [
                                        'name' => $request->nama,
                                        'data' => $data,
                                    ]);

    }

    public function NonRacikan_pdf(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name'     => 'required',
                'signa'    => 'required',
                'qty'      => 'required|numeric'
            ]);

        if ($validator->fails()) {
            return response()->view('errors.404',$data,404);
        }

        $pdf = PDF::loadview('nonracikan_pdf',  [
                                                    'obatalkes' => $request->name, 
                                                    'signa'     => $request->signa,
                                                    'qty'       => $request->qty
                                                ]);

        return $pdf->download('Nonracikan-pdf');
    }

    public function Racikan_pdf(Request $request)
    {
        $arrayObat  = $request->get('obat');
        $arraySigna = $request->get('signa');
        $arrayQty   = $request->get('qty');
        $data = array();
        for ($i=0; $i < count($arrayObat); $i++) { 
            $dt_obat    = Obatalkes::select('obatalkes_nama','stok')
                           ->where('obatalkes_kode', '=', $arrayObat[$i])
                           ->first();

            $dt_signa   = Signa::select('signa_nama')
                               ->where('signa_kode', '=', $request->signa)
                               ->first();

            if ($dt_obat->stok < $arrayQty[$i]) {
                return back()->with('error', 'Stok obat tidak cukup.');
            }

            $detail = array(
                                'kode_obat' => $arrayObat[$i],
                                'obat'      => $dt_obat->obatalkes_nama,
                                'kode_signa'=> $arraySigna[$i],
                                'signa'     => $dt_signa->signa_nama,
                                'qty'       => $arrayQty[$i]
                            );
            array_push($data, $detail);
        }

        $pdf = PDF::loadview('racikan_pdf', [
                                                'name' => $request->name,
                                                'data' => $data
                                            ]);

        return $pdf->download('Racikan-pdf');
    }
}
