<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Obatalkes;
use App\Models\Signa;
use DataTables;

class ObatalkesController extends Controller
{
    public function index()
    {
    	return view('page/main');
    }

    public function getObatalkes(Request $request)
    {
        if ($request->ajax()) {
            $data = Obatalkes::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function resepRacikan()
    {
        $obatalkes = Obatalkes::all();
        $signa     = Signa::all();
        return view('page/resepracikan', ['obatalkes' => $obatalkes, 'signa' => $signa]);
    }

    public function resepNonRacikan()
    {
        $obatalkes = Obatalkes::all();
        $signa     = Signa::all();
        return view('page/resepnonracikan', ['obatalkes' => $obatalkes, 'signa' => $signa]);
    }    

    public function getStockObat($kode)
    {
        $obatalkes = Obatalkes::select('stok')
                           ->where('obatalkes_kode', '=', $kode)
                           ->first();

        return $obatalkes->stok;
    }
}
