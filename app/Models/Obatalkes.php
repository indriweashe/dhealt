<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Obatalkes extends Model
{
	const UPDATED_AT = 'last_modified_date';
    protected $table = "obatalkes_m";

}
